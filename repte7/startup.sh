#! /bin/bash

# Copiem els arxius echo, chargen i daytime al directori en específic.
cp /opt/docker/index.html /var/www/html/index.html
cp /opt/docker/daytime-stream /etc/xinetd.d
cp /opt/docker/echo-stream /etc/xinetd.d
cp /opt/docker/chargen-stream /etc/xinetd.d

# Engeguem servei xinetd
xinetd -dontfork

# Engeguem servei apache en FOREGROUND
apachectl -k start -X

