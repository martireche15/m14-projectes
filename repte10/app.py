import time
import redis #base de dades clau-valor
from flask import Flask #mecanisme per desenvolupar aplicacions web de forma ràpida

#una variable per crear l'aplicació i una altre per contactar amb el redis
app = Flask(__name__) 
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

#Programa principal:
#Ruta de l'aplicació arrel (index)
@app.route('/')
#funció hello que compta les visites
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times :)\n'.format(count)

@app.route('/patata')
#funció hello que compta les visites
def patata():
    count = get_hit_count()
    return 'lewandoosky'

