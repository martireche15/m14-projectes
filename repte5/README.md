# Servidor LDAP amb persistència de dades

>[!NOTE]
>
>Apunts: [*HowTo-ASIX-Docker.pdf*](https://gitlab.com/edtasixm05/docker/-/blob/master/HowTo-ASIX-Docker.pdf?ref_type=heads) capítol “Docker bàsic”.

Implementar una imatge de servidor ldap configurable segons el paràmetre rebut.
S’executa amb un entrypoint que és un script que actuarà segons li indiqui
l’argument rebut:

initdb → ho inicialitza tot de nou i fa el populate de edt.org

slapd → ho inicialitza tot però només engega el servidor, sense posar-hi dades

start / edtorg / res → engega el servidor utilitzant la persistència de dades de la bd i de la configuració. És a dir, engega el servei usant les dades ja existents

slapcat nº (0,1, res) → fa un slapcat de la base de dades indicada 


## Desenvolupament

### Creem un volum on volem que es guardin les dades i un altre per a la configuració:
```
a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-config
ldap-config

a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-data
ldap-data
```
### Creem el Dockerfile
```Dockerfile
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
EXPOSE 389
```
Dins del Dockerfile, cambiem el nostre CMD per un ENTRYPOINT que llegira la informació del nostre startup.

### Creem el startup.sh
```startup.sh
#!/bin/bash

arg2=$2
case "$1" in
  "slapd")
    echo "Engegant servidor."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "initdb")
    echo "Creant Servidor LDAP amb les dades de l'escola."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "start"|"edtorg"|" ")
    echo "Iniciando el servicio LDAP amb les dades per defecte."
    echo "Creant Servidor LDAP amb les dades de l'escola."
    rm -rf /etc/ldap/slapd.d/*
    rm -rf /var/lib/ldap/*
    slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
    slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
    chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
    /usr/sbin/slapd -d0
    ;;
  "slapcat")
    if [ $arg2 == "0" ] || [ $arg2 == "1" ]; then
  	slapcat -n$arg2
    elif [ -z "$arg2" ]; then
	  slapcat
    fi
    ;;

  *)
    echo "Uso: $0"
    exit 1
    ;;
esac
```
Dins del fitxer startup tenim una estructura case que rep el argument un primer argument, depenent d'aquest, engega el container d'una manera o altra. Si el primer argument es un slapcat, tindrem en compte un segon argument (en cas que tingui) i fara un slapcat d'una base de dades o d'una altra.

### Creem els fitxers slapd.conf i edt-org.ldif
Aquest dos fitxer els importarem del github *edtasixm06*:

El fitxer slapd.conf contindra la configuració principal del servidor LDAP OpenLDAP:
>Fitxer: [*edt-org.ldif*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/edt-org.ldif)

El fitxer edt-org.ldif contindra una base de dades:
>Fitxer: [*slapd.conf*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/slapd.conf)

### Creem la imatge
```
a221826mr@i16:~/m14-projectes/repte4$ docker build -t martireche15/repte5
```

### Iniciem la imatge en un container utilitzant els volums creats anteriorment
El volum ldap-config l'associem al directori de configuració: /etc/ldap/slapd.d.
El volum ldap-data l'associem al directori de dades: /var/lib/ldap
```
a221826mr@i16:~/m14-projectes/repte4$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap martireche15/repte4:ldap
```

### Comandes per executar:
initdb:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 initdb
```
slapd:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapd
```
start|edtorg|res:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d/ -v ldap-dades:/var/lib/ldap/ -p 389:389 repte5 start
```
Per comprobar que hi ha persistencia:
```
# Entrem interactivament al container i esborrem un usuari:
docker exec -it ldap.edt.org /bin/bash
ldapdelete -vx -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Jordi Mas,ou=usuaris,dc=edt,dc=org'

# Sortim i tornem a entrar al container utilitzant els volums i busquem l'usuari esborrat: 
ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=Jordi Mas'
```
slapcat:
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapcat
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 388:389 -it repte5 slapcat 0
```
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -it repte5 slapcat  1
```
