# POSTGRES + ADMINER

## Creem el fitxer yml
**compose.yml**
```
# Use postgres/passwd credentials
services:
  postgres:
    image: martireche15/training
    container_name: postgres
    hostname: postgres.edt.org
    restart: always
    environment:
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    networks:
      - mynet
  
  adminer:
    image: adminer
    container_name: adminer
    hostname: adminer.edt.org
    restart: always
    ports:
      - 8080:8080
    networks:
      - mynet
networks:
  mynet:
```
He utilitzat les imatges ***martireche15/training*** i ***adminer*** per a fer el docker compose.

### Contingut imatge martireche15/training
**dockerfile**
```
#postgres server

FROM postgres:latest
LABEL author="@marti reche"
LABEL subjecte="postgres_server"
COPY trainingv7.sql /docker-entrypoint-initdb.d/
```
El fitxer ***trainingv7.sql*** crea la base de dades "training" a utilitzar.







