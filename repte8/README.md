# LDAP + PHPLDAPADMIN

## Creem el fitxer yml
**compose.yml**
```
version: "2"
services:
  ldap:
    image: martireche15/ldap23:acl
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: martireche15/phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
```
## Creació imatge phpldapadmin a utilitzar
**Dockerfile**
```dockerfile
# phpldapadmin
FROM fedora:27
LABEL version="1.0"
LABEL author="@Marti Reche"
LABEL subject="phpldapadmin"
RUN dnf -y install phpldapadmin php-xml httpd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 80
```
**startup**
```startup
#! /bin/bash
# copiar arxius de configuracio php amb ldap
cp /opt/docker/phpldapadmin.conf /etc/httpd/conf.d/phpldapadmin.conf
cp /opt/docker/config.php  /etc/phpldapadmin/config.php

# engegar php i http
/sbin/php-fpm
/usr/sbin/httpd -D FOREGROUND
```
Els fitxers de configuració de phpldapadmin utilitzats son:
> - phpldapadmin.conf
> - config.php
>
> El seu contingut el podem trobar executant la imatge ***martireche15/phpldapadmin*** en un container


## Execució docker compose
```
# Engegar
- docker compose up -d

# Aturar
- docker compose down / ^C
```


ldapsearch -x -LLL
ldapsearch -x -LLL -b 'dc=edt,dc=org' -h localhost

vim /etc/openldap/ldap.conf 
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-master.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

TLS_CACERTDIR	/etc/openldap/certs

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on

URI ldap://ldap.edt.org
BASE dc=edt,dc=org
```
ldapsearch -x -LLL

$ ldapsearch -x -H ldap://172.22.0.1 -b 'dc=edt,dc=org'

vim config.php
```
# Canviem linia 299:
servers->setValue('server','host','ldap.edt.org');
```





