#! /bin/bash
# copiar arxius de configuracio php amb ldap
cp /opt/docker/phpldapadmin.conf /etc/httpd/conf.d/phpldapadmin.conf
cp /opt/docker/config.php  /etc/phpldapadmin/config.php

# engegar php i http
/sbin/php-fpm
/usr/sbin/httpd -D FOREGROUND

