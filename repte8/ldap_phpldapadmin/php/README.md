# IMATGE PHPLDAPADMI

## **DOCKERFILE** 
```dockerfile
# phpldapadmin
FROM fedora:27
LABEL version="1.0"
LABEL author="@Marti Reche"
LABEL subject="phpldapadmin"
RUN dnf -y install phpldapadmin php-xml httpd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 80
```
## **STARTUP**
```startup
#! /bin/bash
# copiar arxius de configuracio php amb ldap
cp /opt/docker/phpldapadmin.conf /etc/httpd/conf.d/phpldapadmin.conf
cp /opt/docker/config.php  /etc/phpldapadmin/config.php

# engegar php i http
/sbin/php-fpm
/usr/sbin/httpd -D FOREGROUND
```
Els fitxers de configuració de phpldapadmin utilitzats son:
> - phpldapadmin.conf
> - config.php
>
> El seu contingut el podem trobar executant la imatge ***martireche15/phpldapadmin*** en un container


