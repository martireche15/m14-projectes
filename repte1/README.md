# Imatge personalitzada de Debian per fer inspeccions al sistema

Aquesta imatge de Docker està basada en un Debian i conté eines útils per inspeccionar el sistema

## Eines
- "ps"
- "ping"
- "ip"
- "nmap"
- "tree"
- "vim"
- "iputils-ping"

## Dockerfile

L'arxiu "Dockerfile" definirà els passos per a la construcció de la imatge Docker. Resum del contingut:

### Contingut del Dockerfile:

```dockerfile
# utilització d'una imatge Debian com a base
FROM debian

# Actualitza els repositoris i instal·la els paquets necessaris
RUN apt-get update && apt-get install -y \
 procps iproute2 nmap tree vim iputils-ping

# Establim el CMD per a que al executar el contenidor sigui interactiu
CMD /bin/bash
```

## Desenvolupament

Per crear aquesta imatge, utilitzarem aquesta ordre:

```bash
docker build -t martireche15/repte1 .
```

## Ús

Per executar un contenidor interactiu amb aquesta imatge, podem utilitzar la següent ordre:

```bash
docker run -it martireche15/repte1 .

