# Projecte 6 Crear una màquina virtual Windows

### Descripció

En aquesta pràctica es crea una màquina virtual windows i s’aprèn a accedir-hi tant
via consola a través de SSH (o Putty) com a través d’un client d’escriptori remot com
per exemple Remmina.

Aprendre a accedir amb un client d’escriptori remot a l’entorn gràfic del windows
seguint el procediment per obtenir les credencials d’accés, amb les claus SSH i la
desencriptació i generació del password.

6.1 Generar una màquina virtual Windows i accedir-hi amb un client d’escriptori
remot.
6.2 generar una màquina virtual Windows i accedir-hi amb un client SSH i/o amb
Putty.
6.3 Crear una màquina virtual Windows que ofereix algun tipus de servei de xarxa.
6.4 Generar un domini AD Windows amb un client del domini.

## Apartat 6.1 Crear una instància Windows

### Procediment

+ Crear la instància Windows
    + Seleccionar Launch Instance
    + En seleccionar Windows informa que canviarà el Security Groups, això és
perquè ara l’accés ha de permetre RDP remote Desktop protocol, per
permetre accedir a través d’un client d’escriptori remot.
    + Seleccionar: Name, OS, Instance Type, Key pair, Network Settings
(permetent RDP i observant el VPC assignat) i Configure Storage (30 GiB per
a Window, ara segur que ens passem de la mida del free-tier, però poquet...).
    + Sempre podem clicar els botons de Edit (per exemple a Network Settings) i
mirar en detall què s’està configurant.
82
+ Accedir a la instància Windows usant RDP
    + Connect.
    + Seleccionar A standalone RDP client.
    + Download a remote desktop file.
    + Get password
        + Observar el Key name.
        + Key Pair Path per accedir al fitxer amb la clau privada, aquest fitxer
servirà per generar un password secret amb el que connectar.
        + Decrypt password. Observar el password generat i copiar-lo.
    + Obrir el fitxer rdp amb l’aplicació remmina, si no està instal·lada cal
instal·lar-la.
    + Pot ser que indiqui que cal acceptar el certificat.
    + Verificar el nom d’usuari i copiar el passwd obtingut anteriorment. Sovint
aquest pas genera problemes segons com es copia, usar el símbol que
apareix de copiar automàticament en clicar el get password.
    + Ja tenim un windows!. Clicant a Toggle Dynamic Resolution Update de
Remmina es pot redimensionar l’escriptori.



