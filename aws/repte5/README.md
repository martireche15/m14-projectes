# Projecte 5 Assignar una adreça IP elàstica a una màquina virtual

### Descripció

Gestionar adreces IP flotants, associades a un compte d’usuari en el ‘free-tier’.
Observar la creació, assignació i destrucció d’adreces IP flotants.

Crear una adreça flotant al compte de l’usuari. Assignar després aquesta adreça a
un dels recursos, màquines virtuals, de l’usuari, per exemple al servidor web
Apache.

Verificar des d’un client extern, per exemple el host de l’aula, que es pot accedir al
servei web sempre a través de la mateixa adreça IP, amb independència de si la VM
s’ha reiniciat.

5.1 Assignar una adreça IP flotant .
5.2 Associar l'adreça IP flotant a una altra instància

## Apartat 5.1 Assignar una adreça IP Flotant

### Procediment

+ Crear una adreça IP: Allocate Elastic IP address
+ Associar l’adreça IP a una instància: Associate IP address
+ Accions:
    + View details
    + Release Elastic IP address
    + Associate Elastic IP address
    + Disassociate Elastic IP address
    + Update reverse DNS

### Seguiment dels passos


