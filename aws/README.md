# AWS 

link [awsacademy](https://awsacademy.instructure.com/)

## Crear una instancia

+ Entrem al link i fem click a **Student Login**
+ Introduïm username/email i passwordç
+ Ens situem a **Dashboard** i entrem a **AWS Academy Learner Lab**
+ Entrem a **Modules**
    + Launch AWS Academy Learner Lab
+ Start Lab
    + **AWS** un cop iniciada i tot seguit entrem a **EC2**
+ Entrem a **EC2 Dashboard** 
    + **Launch instance** (crear la instancia)

Un cop creada la instancia, podem accedir a ella des de l'apartat **Instances** del EC2 service.
Despleguem menú **Instance state**:

+ Stop instance
+ Start instance
+ Reboot instance
+ Hibernate instance
+ Terminate instance

Si seleccionem la instancia podem veure les seves característiques, com la seva IPv4 (que canvia cada cop que s'inicia l'instancoia).


