# Projecte 4 Crear una Imatge AMI personalitzada

### Descripció
Crear una imatge AMI propia personalitzada utilitzant alguna de les imatges
generades en les pràctiques anteriors. Per exemple crear una imatge que incorpori
el servidor web Apache i la nostra seu web o crear una altra AMI amb el servei
docker instal·lat i amb les imatges dels serveis web i net descarregades.

Crear noves màquines virtuals basades en aquestes AMI creades. Observar els
conceptes de AMI, Community i Marketplace.

Identificar l’emmagatzemament utilitzat per les noves AMI personalitzades creades
com a EBS Elastic Block Storage. Gestionar EBS.

4.1 Crear una AMI basada en el servidor web Apache.
4.2 Crear una AMI basada en la màquina que incorpora docker i els serveis web i
net.

## Apartat 4.1 Crear una imatge AMI amb una seu web
### Procediment
+ Seguir els passos descrits a l’apartat 3.1 Crear un servidor web Apache.
+ Crear una nova AMI.
    + Assignar-li nom
    + Identificar l'espai d’emmagatzemament que ocupa la imatge.
    + És un Volume EBS Elastic Volume Store.
    + Tarden una estona a crear-se, cal duplicar els GB de disc dur...
    + Pot ser que en el procés si tenim una sessió SSH oberta aquesta es tanqui. Passat un temps es pot tornar a connectar.

+ Gestionar AMIs
    + Launch
    + Spot Request
    + Deregister
    + Register New AMI  
    + Copy AMI
    + Modify Image Permissions
    + Add / Edit Tags
    + Modify Boot Volume Setting
    + EC2 Image Builder

## Apartat 4.2 Crear una AMI personalitzada (compose / passwd / etc)

### Procediment

+ Crear una màquina virtual amb un servidor Apache, amb docker instal·lat i amb el
servei net21 desplegat. Podem utilitzar l’apartat 3.3 Afegir Docker a la màquina
virtual i desplegar un servei.
    + Servei Apache instal·lat i actiu amb una pàgina web ‘senzilla’ per defecte.
    + Servei docker instal·lat i actiu.
    + desplegat en detach el container edtasixm05/net21:base fent propagació dels
ports 7, 13 i 19.

+ Personalitzar-la seguint els passos següents:
    + Assignar un password a root.
    + Assignar un passwd a l’usuari per defecte ec2-user.
    + Descarregar al home de l’usuari els repositoris de gitlab de
edtasixm05/docker i edtasixm05/aws.
    + Instal·lar Docker Compose (si no està instal·lat) manualment a través de
l’executable docker-compose.

+ Generar una nova AMI basada en aquest imatge: myAMI

+ Generar una instància basada en la AMI i verificar-ne els serveis
    + Atenció: Seleccionar el Keypair apropiat
    + Atenció: Seleccionar el Security Groups apropiat (mylinux)

### Seguiment dels passos

entrem a la instance myAMI
```
ssh -i .ssh/vockey.pem ec2-user@54.152.155.58
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo passwd
Changing password for user root.
New password: jupiter
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: jupiter
passwd: all authentication tokens updated successfully.
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo passwd ec2-user 
Changing password for user ec2-user.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo yum install git nmap tree
```

```
[ec2-user@ip-172-31-18-73 ~]$ git clone https://www.gitlab.com/edtasixm05/aws

[ec2-user@ip-172-31-18-73 ~]$ git clone https://www.gitlab.com/edtasixm05/docker
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo curl -SL https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100 24.7M  100 24.7M    0     0  78.3M      0 --:--:-- --:--:-- --:--:-- 78.3M
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo chmod +x /usr/local/bin/docker-compose
```

```
[ec2-user@ip-172-31-18-73 ~]$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

```
[ec2-user@ip-172-31-18-73 ~]$ docker-compose ls
NAME                STATUS              CONFIG FILES
```

```
[ec2-user@ip-172-31-18-73 ~]$ docker run --name net.edt.org -h net.edt.org --net mynet --restart unless-stopped -p 7:7 -p 13:13 -p 19:19 -d edtasixm05/net21:base
bf2a620c3df4fdfb2d76b5000bd2cbece703722f9bfd9550c01858a66a143429
```

```
[ec2-user@ip-172-31-18-73 ~]$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                                                                 NAMES
bf2a620c3df4   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   20 seconds ago   Up 19 seconds   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp   net.edt.org
```

#### Generem la nova AMI myAMI_repte4
    + click dret des de la instancia actual
    + images and templates
    + create image

#### Un cop creada la nova AMI, fem **launch instance from AMI**.

#### Entrem a la instància per ssh i fem les comprovacions:

```
[ec2-user@ip-172-31-25-180 ~]$ docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS                                                                                                 NAMES
7ade0726f0ee   edtasixm05/net21:base   "/usr/sbin/xinetd -d…"   8 minutes ago   Up 2 minutes   0.0.0.0:7->7/tcp, :::7->7/tcp, 0.0.0.0:13->13/tcp, :::13->13/tcp, 0.0.0.0:19->19/tcp, :::19->19/tcp   net.edt.org
```

```
[ec2-user@ip-172-31-25-180 ~]$ docker-compose version
Docker Compose version v2.6.0
```
```

[ec2-user@ip-172-31-25-180 ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; preset: disabled)
     Active: active (running) since Tue 2023-12-12 10:45:55 UTC; 11min ago
       Docs: man:httpd.service(8)
   Main PID: 1982 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 177 (limit: 1114)
     Memory: 17.8M
        CPU: 452ms
     CGroup: /system.slice/httpd.service
             ├─1982 /usr/sbin/httpd -DFOREGROUND
             ├─2034 /usr/sbin/httpd -DFOREGROUND
             ├─2035 /usr/sbin/httpd -DFOREGROUND
             ├─2037 /usr/sbin/httpd -DFOREGROUND
             └─2050 /usr/sbin/httpd -DFOREGROUND
```

#### Des d'un host local de l'aula:
```
a221826mr@i16:~$ telnet 54.87.25.24 13
Trying 54.87.25.24...
Connected to 54.87.25.24.
Escape character is '^]'.
12 DEC 2023 11:00:06 UTC
Connection closed by foreign host.
```

```
a221826mr@i16:~$ telnet 54.87.25.24 7
Trying 54.87.25.24...
Connected to 54.87.25.24.
Escape character is '^]'.
vinicius
vinicius
^]
telnet> quit
Connection closed.
```

```
a221826mr@i16:~$ telnet 54.87.25.24 19 | head -n10
Trying 54.87.25.24...
Connected to 54.87.25.24.
Escape character is '^]'.
!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefgh
"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghi
#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghij
$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijk
%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijkl
&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklm
'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmn
```










