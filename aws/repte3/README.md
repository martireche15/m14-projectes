# Projecte 3 Generar un servidor web amb Apache

### Descripció

Crear un servidor web bàsic usant Apache i una sola pàgina índex ben simple.
Caldrà crear una màquina virtual GNU/Linux, accedir-hi via SSH i instal·lar
manualment els paquets del servei Apache. També generar manualment la seu web.

Aprendre a configurar el Security Groups per filtrar el tràfic entrant i sortint de la
màquina virtual. Cal permetre el tràfic web entrant provinent de qualsevol lloc o d’un
origen seleccionat.

Des de l’exterior, un host de l’aula, es verifica l’accés al servei web desplegat al
Cloud.

Finalitzar alliberant (tancat o eliminant) tots aquells recursos que siguin innecessaris.

* 3.1 Crear una VM amb un servei web Apache i configurar els ports.
* 3.2 Gestionar Security Groups
* 3.3 Crear una VM, instal·lar-hi docker i desplegar algun tipus de servei de xarxa, per
* exemple el servei web o el servei net. Configurar els ports apropiadament.
* 3.4 Repetir l’exemple anterior en algun altre tipus de GNU/Linux, combinant
experiències amb les distribucions: Fedora, Debian, AMI AWS i altres.

---

## Apartat 3.1 Crear un servidor web amb Apache
### Procediment

* Engegar una màquina virtual Linux. Per exemple una AMI Amazon 2.
* Instal·lar el software apache.
* Generar una pàgina web per defecte ‘senzilla’.
* Obrir el port 80 del security groups.
    * Edit inbound rules.
    * Obrir el port per 80 TCP.
    * Permetre l’accés des de qualsevol host client.
* Verificar l’accés extern al servidor web

---

### Seguiment dels passos
#### Iniciem la instància 
Accedim a link [awsacademy](https://awsacademy.instructure.com/) i iniciem la instància

#### Entrem a AWS Details per averiguar la IPv4 de la máquina (en aquest cas 52.204.180.229)

#### Copiem la keypair al nostre ordinador
des del lab:
```
eee_W_2645546@runweb105230:~$ cat .ssh/labsuser.pem
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAgaYDTVlhzraWv2vaawGdXvwg9mGQCrvaxn79DI/vQ2Bnluwv
+CHK5EMG1R03MYsEKNWQWwSBB5Z1Y2zwicPu7Jp4l0uNjMvx9eDxNYRnggB8epCR
qacaVdxF+qLKsiHQCKCnrdR7MufudXBz08tM3wYfbxIivHOxTqB3XVVgSdNKQk4i
rnLbjkBk/Qpy/x5ehNQY3G7lJX5peDZ++D9GrT6IxMO+/HKWI34VlVLBo4b/JFod
NMQUukOhSTiox5NvVLycwNuwLSOy9TCxy4Da9ZfCRkjsIOLtNSYj3mB0VFH2lyfz
7wcNbl3jDZSyCmNiWSQOurl+czS4TzcwUrO4swIDAQABAoIBADfv3LTev6hkdx4X
9qD8jV2SAtbKuU8oRfgmih+FaoHzcj5XoUq5qcRadvQ0KsAhbqX/nszmYJVZgURo
mE+YTdmfvC/q1SPOSpL/fpLMkawxcYOgJHjCNEafR0IgQrwggpV2W2JQrucD0oqw
/gh/SqdfCOjvNjWA5Rs7eMS4NVckQoVVdQqCYKgINb5Zs16TZSBhqfbEJtiPlkq8
lGvWDVNopTXrtwRpaseWn29fwt9+I0O4ofJpeMc4h6295YRkVGLiPp+vjY/OU9R0
moxoKbiUaQPMVi7lRP1L1kv63DUuaPrODZ35sysjua3/CKdhGxjFEGFkmTpdreBP
2fa6kMECgYEA9B2AZ+0KDB/3yPWMyaNtSBs3GMlEpzQhlEMvVBbgtyn/52oduE7a
O4K89BAnAqsVp56x2Qnx8tdA0RMlo0IatH3z/dBwKxq2bhfZcUTPLBZkbxrEkLQn
YosLXa830E091FzXlXbHDkGpJPMnca4MEFY1QwN/16F+Jrtzcgl0n6sCgYEAh/Xe
sU3IK6WvFJBi9LfL2kxvdR24cQjz0kCzA0IKIllc5mRPNidz7fm4fAnLpg/KuvyD
miYniIxpDoQHnue0Z3r+D0uNVTPIBpY2MoCDWHUb6njb3pm3RPbxtezywH3pKe4z
LSU3Z1HfQD4OKhpqM67XZAw2MAJJYT/EhPizYxkCgYBao/GfpQYMBnqspXAHFqVn
FqPYkjLpmpO1R1T1diEuIWwhcudiHfUuD5c1UCIL/ZZmzKfgnK63qnEosGFTQyJD
TlkgLxrXBm4iZryYMCJ0jkPwdRHzYNEW5+XqfTrOH0AL8AUkPBwhEQRK8fX9YdCc
6EWSgmQfnpkGrKdq+lPRTQKBgEtE6QaLozfWv/jIfqAwaKDwE/xoecXKSQGrY+93
zhh6FJ5QBjJa4SlsSKAS5DWJJJkkVs8uNFC6OycYHQfyCsQHKUPadeIniqNq4ZJP
52c/XjcOtdqkivuZIvOuvC2pBz55asehhOykPM5aDIlumkEGmKQAxrcJbQpzvR5I
eTdxAoGAXsB4AzOQvZFObUwCqgw+vELrBOlk6tKLkx6jBeXUjTQdd27jUM80SxUX
ETgBDxQ5HVhsuGqPsdxiUwK/h+4WrfoIGGnpB7kMR5J/hZSkioOsEy3WokGUM4Br
GrDIqJgha4LGE8WVcFOadIwYKbL1n6SNlVWXNKctUo5cB0f0l6U=
```

des del pc:
```
$ vim mykey.pem
aquest fitxer conté la clau privada
```

canviem els permisos de la nova keypair:
```
$ chmod 400 mykey.pem
```

---

### Entrem a la màquina:
```
$ ssh -i vockey.pem ec2-user@52.204.180.229
```

#### Creem el contingut de la pàgina web
```
[ec2-user@ip-172-31-30-62 ~]$ sudo vim /var/www/html/index.html
[ec2-user@ip-172-31-30-62 ~]$ sudo vim /var/www/html/grid.css
[ec2-user@ip-172-31-30-62 ~]$ ls /var/www/html/
grid.css  index.html
```

#### Iniciem el servei httpd
```
[ec2-user@ip-172-31-30-62 ~]$ sudo systemctl enable httpd
[ec2-user@ip-172-31-30-62 ~]$ sudo systemctl start httpd
[ec2-user@ip-172-31-30-62 ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
 	Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; preset: disabled)
 	Active: active (running) since Fri 2023-12-01 11:44:21 UTC; 5s ago
   	Docs: man:httpd.service(8)
   Main PID: 3232 (httpd)
 	Status: "Started, listening on: port 80"
  	Tasks: 177 (limit: 1114)
 	Memory: 17.5M
    	CPU: 77ms
 	CGroup: /system.slice/httpd.service
         	├─3232 /usr/sbin/httpd -DFOREGROUND
         	├─3233 /usr/sbin/httpd -DFOREGROUND
         	├─3234 /usr/sbin/httpd -DFOREGROUND
         	├─3235 /usr/sbin/httpd -DFOREGROUND
         	└─3236 /usr/sbin/httpd -DFOREGROUND

Dec 01 11:44:21 ip-172-31-30-62.ec2.internal systemd[1]: Starting httpd.service - The Apache HTTP Server...
Dec 01 11:44:21 ip-172-31-30-62.ec2.internal systemd[1]: Started httpd.service - The Apache HTTP Server.
Dec 01 11:44:21 ip-172-31-30-62.ec2.internal httpd[3232]: Server configured, listening on: port 80
```

#### Modificae el security group
+ create security group
+ accedim a **inbound rules**
    + SSH port 22 amb accés per a tothom 0.0.0.0/0
    + HTTP port 80 amb accés per a tothom 0.0.0.0/0
+ save rules
+ assignem el security group a la instància ***myweb***

```
$ wget 52.204.180.229
--2023-12-04 13:39:07--  http://52.204.180.229/
Connecting to 52.204.180.229:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 344326 (336K) [text/html]
Saving to: ‘index.html’

index.html                         100%[================================================================>] 336,26K   743KB/s    in 0,5s    

2023-12-04 13:39:07 (743 KB/s) - ‘index.html’ saved [344326/344326]
```
```
$ cat index.html
```
*Visualitzem la pàgina web amb la IPv4 de la instància*

--- 

## Apartat 3.2 Gestionar Security Groups
### Procediment

+ Security Group: default
    + Inbound: All, Outbound: All
+ Atenció en crear màquines: launch-wozard-nº
    + Inbound: SSH, Outbound: All
+ Actions:
    + View details
    + Edit Inbound Rules
    + Edit outbound Rules
    + Manage Tags
    + Manage Stale Rules
    + Copy to new security group
    + delete security group
+ Edit rules
    + Type
    + Protocol
    + Port range
    + Source
    + Description

---

## Apartat 3.3 Afegir Docker a la màquina virtual i desplegar un servei
### Procediment

+ Identificar el tipus de sistema operatiu.
+ Identificar si ja porta docker incorporat o no.
+ Identificar si ja incorpora el repositori de docker no.
+ Segons correspongui al sistema operatiu procedir a la instal·lació de docker.
+ Configurar:
    + Establir docker com a servei actiu per defecte
    + Afegir l’usuari al grup docker.
+ Desplegar un servei docker.
    + Crear una xarxa propia
    + Desplegar el servei edtasixm05/net21:base i propagar els ports 7, 13 i 19.
+ Obrir els ports al Security Group
    + Crear un nou security group anomenat mylinux
    + Obrir els ports 22, 80, 7, 13 i 19.
    + Verificar que està assignat a la màquina virtual

### Seguiment de passos

```
[ec2-user@ip-172-31-30-62 ~]$ uname -a
Linux ip-172-31-30-62.ec2.internal 6.1.61-85.141.amzn2023.x86_64 #1 SMP PREEMPT_DYNAMIC Wed Nov  8 00:39:18 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux
```

```
[ec2-user@ip-172-31-30-62 ~]$ cat /etc/os-release 
NAME="Amazon Linux"
VERSION="2023"
ID="amzn"
ID_LIKE="fedora"
VERSION_ID="2023"
PLATFORM_ID="platform:al2023"
PRETTY_NAME="Amazon Linux 2023"
ANSI_COLOR="0;33"
CPE_NAME="cpe:2.3:o:amazon:amazon_linux:2023"
HOME_URL="https://aws.amazon.com/linux/"
BUG_REPORT_URL="https://github.com/amazonlinux/amazon-linux-2023"
SUPPORT_END="2028-03-15"
```

```
[ec2-user@ip-172-31-30-62 ~]$ yum repolist
repo id                                                 repo name
amazonlinux                                             Amazon Linux 2023 repository
kernel-livepatch                                        Amazon Linux 2023 Kernel Livepatch repository
[ec2-user@ip-172-31-30-62 ~]$ 
```

```
[ec2-user@ip-172-31-30-62 ~]$ sudo yum install docker

[ec2-user@ip-172-31-30-62 ~]$ systemctl enable docker

[ec2-user@ip-172-31-30-62 ~]$ systemctl start docker

[ec2-user@ip-172-31-30-62 ~]$ systemctl status docker
```

#### Afegim l'usuari ec2-usuer al grup docker
```
[ec2-user@ip-172-31-30-62 ~]$ sudo usermod -aG docker ec2-user
```
```
[ec2-user@ip-172-31-30-62 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

[ec2-user@ip-172-31-30-62 ~]$ docker network create mynet
289fcf50e13b6431ba931fff4f47f579b778470a5a025924cfe4bfdeb0abef34

[ec2-user@ip-172-31-30-62 ~]$ docker run --rm --name net.edt.org -h net.edt.org --net mynet -p 7:7 -p 13:13 -p 19:19 -d edtasixm05/net21:base

[ec2-user@ip-172-31-30-62 ~]$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 11:45 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00016s latency).
Not shown: 995 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen
22/tcp open  ssh
80/tcp open  http

[ec2-user@ip-172-31-30-62 ~]$ docker network inspect mynet

[ec2-user@ip-172-31-30-62 ~]$ nmap 172.18.0.2
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 11:46 UTC
Nmap scan report for ip-172-18-0-2.ec2.internal (172.18.0.2)
Host is up (0.00021s latency).
Not shown: 997 closed tcp ports (conn-refused)
PORT   STATE SERVICE
7/tcp  open  echo
13/tcp open  daytime
19/tcp open  chargen



```
[ec2-user@ip-172-31-30-62 ~]$ nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2023-12-04 11:25 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00026s latency).
Not shown: 999 closed tcp ports (conn-refused)
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.09 seconds
```

#### Editem el security group
ports 20, 88, 7, 13, 19

$ telnet "ip publica instància" "port"

