# Projecte 7 IAM Roles

### Descripció

Aprendre a treballar amb la gestió d’usuaris, grups, rols i policies de AWS. Crear
usuaris i grups, assignar rols als grups i definir policies d’accés.

Evitar treballar en el compte privat de AWS usant el root account. Definir un (o més)
noms d’usuari amb rols diferents per accedir al compte.

7.1 Crear comptes d’usuari alternatius al Root Account.
7.2 Crear usuaris, grups, roles i policies.
7.3 Realitzar l’exercici dels tutorials de IAM Roles.
7.4 Personalització de policies i roles.

Documentació:

+ [<ins>IAM Identity and Acces Managment<ins/>](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html?icmpid=docs_iam_console)
+ [<ins>HowTo-ASIX-AMazon-AWS-EC2-AMI-Cloud<ins/>](https://gitlab.com/edtasixm05/aws/-/blob/main/HowTo-ASIX-Amazon-AWS-EC2-AMI-Cloud.pdf) apartat "IAM Identity and Access Managment".



