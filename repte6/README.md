# Servidor LDAP amb persistència de dades

>[!NOTE]
>
>Apunts: [*HowTo-ASIX-Docker.pdf*](https://gitlab.com/edtasixm05/docker/-/blob/master/HowTo-ASIX-Docker.pdf?ref_type=heads) capítol “Docker bàsic”.

Implementar una imatge que actuï com a servidor ldap amb persistència de dades tant de configuració com les dades de la base de dades d’usuaris.

## Desenvolupament

### Creem un volum on volem que es guardin les dades i un altre per a la configuració:
```
a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-config
ldap-config

a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-data
ldap-data
```
### Creem el Dockerfile
```Dockerfile
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker

# Definir variables d'entorn
ENV LDAP_ROOTDN="marti"
ENV LDAP_ROOTPW="marti_pw"

CMD /opt/docker/startup.sh
EXPOSE 389
```

### Creem el startup.sh
```startup.sh
#!/bin/bash

# Modificar el archivo de configuración slapd.conf
## Substituim el super usuari "Manager" per la variable $LDAP_ROOTDN, variable que obtindrem de l'ordre "docker run..."
sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
## Substituim la contrasenya del super usuari per la variable $LDAP_ROOTPW, variable que obtindrem de l'ordre "docker run..."
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf

# 1)Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# 3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# 4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# 5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
```

### Creem els fitxers slapd.conf i edt-org.ldif
Aquest dos fitxer els importarem del github *edtasixm06*:

El fitxer slapd.conf contindra la configuració principal del servidor LDAP OpenLDAP:
>Fitxer: [*edt-org.ldif*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/edt-org.ldif)

El fitxer edt-org.ldif contindra una base de dades:
>Fitxer: [*slapd.conf*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/slapd.conf)

### Creem la imatge
```
a221826mr@i16:~/m14-projectes/repte4$ docker build -t martireche15/repte6 .
```

### Iniciem la imatge en un container
```
docker run --name ldap.edt.org -h ldap.edt.org -d martireche15/repte6 
```

### Entrem interactivament al container des d'una terminal
```
docker exec -it ldap.edt.org /bin/bash
```

observem que s'ha cambiat el nom de l'administrador i la contrasenya
```
cat slapd.conf

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=marti,dc=edt,dc=org"
rootpw marti_pw
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=marti,dc=edt,dc=org" read
       by * none

```


