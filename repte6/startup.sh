#!/bin/bash

# Modificar el archivo de configuración slapd.conf
# 1) Substituim el super usuari "Manager" per la variable $LDAP_ROOTDN, variable que obtindrem de l'ordre "docker run..."
sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
# 2) Substituim la contrasenya del super usuari per la variable $LDAP_ROOTPW, variable que obtindrem de l'ordre "docker run..."
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf



# 1)Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# 3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# 4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# 5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0




