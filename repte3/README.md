# Utilització de volums en un servidor Postgres

Apunts: [*HowTo-ASIX-Docker.pdf*](https://gitlab.com/edtasixm05/docker/-/blob/master/HowTo-ASIX-Docker.pdf?ref_type=heads) capítol “Docker bàsic”.
Usar postgres (la nostra versió) i una base de dades persistent. Usar la imatge
postgres original amb populate i persistència de dades. Exemples de SQL injectat
usant volumes.

## Desenvolupament

### Descarreguem la imatge del servidor postgres original:
```
a221826mr@i16:~/m14-projectes/repte3$ docker pull postgres
Using default tag: latest
latest: Pulling from library/postgres
Digest: sha256:1e90f8560705b0daccbd8eb25573627c8452fc9282496433aab1259ae4c85824
Status: Image is up to date for postgres:latest
docker.io/library/postgres:latest
```

### Creem un volum on volem que es guardin les dades i un altre per a la configuració:
```
a221826mr@i16:~/m14-projectes/repte3$ docker volume create postgres-data
postgres-data

a221826mr@i16:~/m14-projectes/repte3$ docker volume create postgres-config
postgres-config
```

### Comprovem que s'ha creat correctament:
```
a221826mr@i16:~/m14-projectes/repte3$ docker volume ls
DRIVER    VOLUME NAME
local     postgres-config
local     postgres-data
```


### Engeguem el sercei postgres en un nou container(detatch) usant el volum de dades:
```
a221826mr@i16:~/m14-projectes/repte3$ docker run --name training -e POSTGRES_PASSWORD=secret -v postgres-data:/var/lib/postgresql/data -d postgres
3fa3dbfca7c2281f8f09f7f0a4388691c7f5aea89d64363036b8004902e9b2b4
```

### Executem el container interactivament
```
a221826mr@i16:~/m14-projectes/repte3$ docker exec -it training psql -U postgres
```
