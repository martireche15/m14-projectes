# Servidor LDAP amb persistència de dades

>[!NOTE]
>
>Apunts: [*HowTo-ASIX-Docker.pdf*](https://gitlab.com/edtasixm05/docker/-/blob/master/HowTo-ASIX-Docker.pdf?ref_type=heads) capítol “Docker bàsic”.

Implementar una imatge que actuï com a servidor ldap amb persistència de dades tant de configuració com les dades de la base de dades d’usuaris.

## Desenvolupament

### Creem un volum on volem que es guardin les dades i un altre per a la configuració:
```
a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-config
ldap-config

a221826mr@i16:~/m14-projectes/repte4$ docker volume create ldap-data
ldap-data
```
### Creem el Dockerfile
```Dockerfile
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
ARG DEBIAN_FRONTEND es una variable d'entorn que fa que s'instal·lin els paquets ldap de forma no interactiva

### Creem el startup.sh
```startup.sh
#!/bin/bash
echo "Configurant el servidor ldap..."

# 1)Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# 3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# 4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# 5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
```
### Creem els fitxers slapd.conf i edt-org.ldif
Aquest dos fitxer els importarem del github *edtasixm06*:

El fitxer slapd.conf contindra la configuració principal del servidor LDAP OpenLDAP:
>Fitxer: [*edt-org.ldif*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/edt-org.ldif)

El fitxer edt-org.ldif contindra una base de dades:
>Fitxer: [*slapd.conf*] (https://github.com/edtasixm06/ldap22/blob/main/ldap22%3Abase/slapd.conf)

### Creem la imatge
```
a221826mr@i16:~/m14-projectes/repte4$ docker build -t martireche15/repte4:ldap .
```

### Iniciem la imatge en un container utilitzant els volums creats anteriorment
El volum ldap-config l'associem al directori de configuració: /etc/ldap/slapd.d.
El volum ldap-data l'associem al directori de dades: /var/lib/ldap
```
a221826mr@i16:~/m14-projectes/repte4$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap martireche15/repte4:ldap
```

### Entrem interactivament al container des d'una terminal
```
a221826mr@i16:~/m14-projectes/repte4$ docker exec -it ldap.edt.org /bin/bash

Veifiquem que el servidor ldap va i té dades:
root@ldap:/opt/docker# ldapsearch -x -H ldap://localhost -LLL -b "dc=edt,dc=org" dn       
>dn: dc=edt,dc=org
>dn: ou=maquines,dc=edt,dc=org
>dn: ou=clients,dc=edt,dc=org
>dn: ou=productes,dc=edt,dc=org
>dn: ou=usuaris,dc=edt,dc=org
>dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org
>dn: cn=Pere Pou,ou=usuaris,dc=edt,dc=org
>dn: cn=Anna Pou,ou=usuaris,dc=edt,dc=org
>dn: cn=Marta Mas,ou=usuaris,dc=edt,dc=org
>dn: cn=Jordi Mas,ou=usuaris,dc=edt,dc=org
>dn: cn=Admin System,ou=usuaris,dc=edt,dc=org

Eliminem un usuari:
root@ldap:/opt/docker# ldapdelete -vx -H ldap://localhost -D "cn=Manager,dc=edt,dc=org" -w secret "cn=Pau Pou,ou=usuaris,dc=edt,dc=org"
>ldap_initialize( ldap://localhost:389/??base )
>deleting entry "cn=Pau Pou,ou=usuaris,dc=edt,dc=org"

Sortim del container, l'aturem i tornem a entrar
Despres comprovem si l'usuari segueix estant:
>root@ldap:/opt/docker# ldapsearch -x -H ldap://localhost -LLL -b "cn=Pere Pou,ou=usuaris,dc=edt,dc=org" 
```


