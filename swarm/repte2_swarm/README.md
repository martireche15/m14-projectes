# Deploy
## Deplopy amb Stack / Service / Swarm

**apunts** a gitlab edtasixm05/docker/3_docker_intermig/deploy

Desplegar amb *docker stack* en un swarm d'un o més nodes l'aplicació 
d'exemple getstarted comptador tot modificant les condicions del deploy.

  * Stack
  * Service
  * Swarm

---

### Docker Stack

Per desplegar un stack cal primerament iniciar un swarm en un o més nodes.
En aquest exemple es desplegarà la pàgina web del comptador de visites de
l'exemple *martireche15/comptador*.

Conceptes/Passos:
 * Iniciar el swarm
 * Desplegar l'stack i observar les diferents ordres de stack
 * Modificar el deploy

#### Iniciar el swarm

```
$ docker swarm init
Swarm initialized: current node (38gtcde7vacaauhll3l198dtj) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-2bvrbfwxb9xkoslk0kl5cw2juvoi973rm5r3pdm5rezxg05xis-a6z8onh8yjtko53h6ceesc8iv 10.200.243.216:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```
####  Desplegar l'stack

```
$ docker stack deploy -c compose.yml myapp
Creating network myapp_webnet
Creating service myapp_visualizer
Creating service myapp_portainer
Creating service myapp_web
Creating service myapp_redis
```

```
$ docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
u235w8s58rnk   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
ve9f5y9tloyl   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
3eqqxkm5p8qp   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
5y51w36wprz5   myapp_web          replicated   2/2        martireche15/comptador:latest     *:80->80/tcp
```

```
docker stack ps myapp
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
owv2kl1lbo7h   myapp_portainer.1    portainer/portainer:latest        i16       Running         Running 2 minutes ago             
ar9paj8r2xz2   myapp_redis.1        redis:latest                      i16       Running         Running 2 minutes ago             
ck2ngaljin1t   myapp_visualizer.1   dockersamples/visualizer:stable   i16       Running         Running 2 minutes ago             
zq2z0hn1croo   myapp_web.1          martireche15/comptador:latest     i16       Running         Running 2 minutes ago             
xaznscq6teat   myapp_web.2          martireche15/comptador:latest     i16       Running         Running 2 minutes ago             
```

```
docker stack rm myapp
Removing service myapp_portainer
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

#### Modificar el deploy

```
docker stack deploy -c compose.yml  myapp
Creating service myapp_redis
Creating service myapp_visualizer
Creating service myapp_portainer
Creating service myapp_web

docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
yj618jyi46un   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
snynjzf6sdw6   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
5flm2wqos9zb   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
dlordm9bpgzd   myapp_web          replicated   2/2        martireche15/comptador:latest     *:80->80/tcp
```

**Passar de 3 rèpliques web a 5 rèpliques**
```
# Modificar el fitxer docke-compose.yml i establir al servei web 5 repliques
```
```
docker stack deploy -c compose.yml  myapp
Updating service myapp_portainer (id: yj618jyi46unro25pa9gy6wox)
Updating service myapp_web (id: dlordm9bpgzd4376tf0rzk51v)
Updating service myapp_redis (id: snynjzf6sdw6u62fj7vrisu6v)
Updating service myapp_visualizer (id: 5flm2wqos9zbwysxb4jtg16r0)

docker stack services myapp
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
yj618jyi46un   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
snynjzf6sdw6   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
5flm2wqos9zb   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
dlordm9bpgzd   myapp_web          replicated   5/5        martireche15/comptador:latest     *:80->80/tcp
```
**Tornem a passar de tindre 2 rèpliques**

### Docker Service

Es poden gestionar els serveis amb l'ordre *docker service* tant si s'han desplegat amb
*docker-compose* com amb *docker swarm*. Les accions principals que permeten són:

 * ls
 * ps
 * scale
 * update
 * rm
 * logs
 * create
 * inspect

```
$ docker service 
create    inspect   logs      ls        ps        rm        rollback  scale     update    
```

```
docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
yj618jyi46un   myapp_portainer    replicated   1/1        portainer/portainer:latest        *:9000->9000/tcp
snynjzf6sdw6   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
5flm2wqos9zb   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
dlordm9bpgzd   myapp_web          replicated   5/2        martireche15/comptador:latest     *:80->80/tcp
```

```
docker service ps myapp_web 
ID             NAME          IMAGE                           NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
50ddbb8wlpr0   myapp_web.1   martireche15/comptador:latest   i16       Running         Running 9 minutes ago             
v0ix81vp4vt2   myapp_web.2   martireche15/comptador:latest   i16       Running         Running 9 minutes ago             
```

```
docker service ps myapp_visualizer 
ID             NAME                 IMAGE                             NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
pnh9g8363oha   myapp_visualizer.1   dockersamples/visualizer:stable   i16       Running         Running 9 minutes ago             
```

```
docker service logs myapp_redis 
myapp_redis.1.uaoynv9o4yl8@i16    | 1:C 29 Nov 2023 08:13:21.451 # WARNING Memory overcommit must be enabled! Without it, a background save or replication may fail under low memory condition. Being disabled, it can also cause failures without low memory condition, see https://github.com/jemalloc/jemalloc/issues/1328. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
myapp_redis.1.uaoynv9o4yl8@i16    | 1:C 29 Nov 2023 08:13:21.451 * oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
myapp_redis.1.uaoynv9o4yl8@i16    | 1:C 29 Nov 2023 08:13:21.451 * Redis version=7.2.3, bits=64, commit=00000000, modified=0, pid=1, just started
myapp_redis.1.uaoynv9o4yl8@i16    | 1:C 29 Nov 2023 08:13:21.451 * Configuration loaded
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.452 * monotonic clock: POSIX clock_gettime
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.453 * Running mode=standalone, port=6379.
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.453 * Server initialized
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.456 * Creating AOF base file appendonly.aof.1.base.rdb on server start
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.463 * Creating AOF incr file appendonly.aof.1.incr.aof on server start
myapp_redis.1.uaoynv9o4yl8@i16    | 1:M 29 Nov 2023 08:13:21.463 * Ready to accept connections tcp
```

```
docker service rm myapp_portainer 
myapp_portainer
```

**scale services**

```
docker service scale myapp_web=4
myapp_web scaled to 4
overall progress: 4 out of 4 tasks 
1/4: running   [==================================================>] 
2/4: running   [==================================================>] 
3/4: running   [==================================================>] 
4/4: running   [==================================================>] 
verify: Service converged 
```

```
docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                             PORTS
snynjzf6sdw6   myapp_redis        replicated   1/1        redis:latest                      *:6379->6379/tcp
5flm2wqos9zb   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable   *:8080->8080/tcp
dlordm9bpgzd   myapp_web          replicated   4/4        martireche15/comptador:latest     *:80->80/tcp
```

```
docker service ps myapp_web
ID             NAME          IMAGE                           NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
50ddbb8wlpr0   myapp_web.1   martireche15/comptador:latest   i16       Running         Running 13 minutes ago                 
v0ix81vp4vt2   myapp_web.2   martireche15/comptador:latest   i16       Running         Running 13 minutes ago                 
n9cfhvxxmze1   myapp_web.3   martireche15/comptador:latest   i16       Running         Running about a minute ago             
k2mazrv06rnt   myapp_web.4   martireche15/comptador:latest   i16       Running         Running about a minute ago             
```

```
docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_webnet
```

---

### Docker swarm

Tal i com s'ha vist es poden posar un o més nodes en un swarm per crear
un cluster de hosts que treballen onjutament. A l'inici de tot
s'ha vost com crear un swarn en el node *leader*. La mateixa instrucció
indica què cal fer en els nodes *worker* per unir-se al swarm.

Per gestionar noes i swarms convé tractar les ordres:
 
 * docker swarm

 * docker node

```
docker swarm 
ca          init        join        join-token  leave       unlock      unlock-key  update      
```

```
docker node 
demote   inspect  ls       promote  ps       rm       update   
```

Apagar el swarm iniciat en aquest exercici
```
$ docker swarm leave --force
Node left the swarm.
```

