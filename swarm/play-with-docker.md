# play with docker
## @edt ASIX-M05 Curs 2021-2022

## Play With Docker


A la web de [Play With Docker](https://www.docker.com/play-with-docker/) es proporcionen 
recursos per poder treballar Docker remotament. Cal disposar d'un compte d'usuari que
es pot crear ad hoc o utilitzar el comte d'usuari creat per donar-se d'alta 
a [Docker Hub](https://hub.docker.com/). Cal seleccionar:

 * [Lab Environment](https://labs.play-with-docker.com/)
 * I validar-se amb un compte vàlid de Docker Hub


#### Descripció de l'entorn

 * L'entorn i les instàncies són vàlides durant 4h.

 * L'entron de treball **NO és segur!**, no utilitzar passwords ni dades sensibles.

 * *ADD New Instance*   Amb aquest botó es poden generar noves instàncies, una instància
   simula un host. Es pot treballar en la consola de la instància. Observem que

    * *node1* Tota instància té un nom assignat, per exemple *node1*

    * *root@192.168.0.13* Tota instància té una adreça IP assignada. El compte d'usuari amb 
      què es treballa és *root*.

  * *Memory* & *CPU* Indica el percentatge d'utilització de memòria i de cpu.

  * *SSH  ssh ip172-18-0-48-cabpu4p4lkkg00du5dl0@direct.labs.play-with-docker.com*
    En realitat tota instància és un container (apareix el ID a la part superior), podem
    accedir des de l'exterior (casa, aula) a la instància via ssh, copiant la ruta
    indicada. 

  * *Delete* Aquest botó elimina la instància

  * *Editor* Aquest botó permet navegar per els fitxers de */root* de la instància (cal
    crear-los abans) i editar-los amb un editor gràfic simple.

  * *Settings* La icona de settings mostra una pantalla resum dels settings de l'entorn
    paly-with-docker.

  * *Templates* La icona amb la clau anglesa, permet desplegar un conjunt de nodes predefinits,
    per poder practicar:
      
     * 1 Manager 1 Worker
     * 5 Managers No Workers
     * 3 Managers 2 Workers

  * *Ports* Permet obrir ports a la instància.

  * *Close Session* Aquest botó finalitza la sessió a play-with-docker.
 

#### Generar una instància i accedir per ssh


Realitzeu les ordres següents dins d'una instància
```
id
ip a
docker version
docker --version
ping 1.1.1.1
uname -a
apk add nmap
nmap localhost
nmap 192.168.0.13
```

Accedir remotament des de l'aula a la instància via SSH
```
$ ssh ip172-18-0-48-cabpu4p4lkkg00du5dl0@direct.labs.play-with-docker.com
The authenticity of host 'direct.labs.play-with-docker.com (40.76.55.146)' can't be established.
RSA key fingerprint is SHA256:UyqFRi42lglohSOPKn6Hh9M83Y5Ic9IQn1PTHYqOjEA.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'direct.labs.play-with-docker.com,40.76.55.146' (RSA) to the list of known hosts.
Connecting to 20.127.62.215:8022
###############################################################
#                          WARNING!!!!                        #
# This is a sandbox environment. Using personal credentials   #
# is HIGHLY! discouraged. Any consequences of doing so are    #
# completely the user's responsibilites.                      #
#                                                             #
# The PWD team.                                               #
###############################################################
[node1] (local) root@192.168.0.13 ~
```

```
$ id
uid=0(root) gid=0(root) groups=0(root),0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)
[node1] (local) root@192.168.0.13 ~

$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN 
    link/ether 02:42:8b:0d:37:ed brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
181277: eth0@if181278: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 9a:19:03:8c:b3:c2 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.13/23 scope global eth0
       valid_lft forever preferred_lft forever
181281: eth1@if181282: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:12:00:30 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.48/16 scope global eth1
       valid_lft forever preferred_lft forever
[node1] (local) root@192.168.0.13 ~

$ hostname
node1
[node1] (local) root@192.168.0.13 ~

$ uname -a
Linux node1 4.4.0-210-generic #242-Ubuntu SMP Fri Apr 16 09:57:56 UTC 2021 x86_64 Linux
[node1] (local) root@192.168.0.13 ~
```

Desplegar els apunts en la instància
```
$ cd /tmp/
[node1] (local) root@192.168.0.13 /tmp

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 253, done.
remote: Counting objects: 100% (75/75), done.
remote: Compressing objects: 100% (74/74), done.
remote: Total 253 (delta 28), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (253/253), 45.01 KiB | 4.50 MiB/s, done.
Resolving deltas: 100% (104/104), done.
[node1] (local) root@192.168.0.13 /tmp

$ docker run --rm -it debian uname -a
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
e756f3fdd6a3: Pull complete 
Digest: sha256:3f1d6c17773a45c97bd8f158d665c9709d7b29ed7917ac934086ad96f92e4510
Status: Downloaded newer image for debian:latest
Linux 8f044bc01058 4.4.0-210-generic #242-Ubuntu SMP Fri Apr 16 09:57:56 UTC 2021 x86_64 GNU/Linux
[node1] (local) root@192.168.0.13 /tmp

$ docker run --rm -it debian cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
[node1] (local) root@192.168.0.13 /tmp
```


#### Generar un swarm amb dos nodes

Aquest exemple desplega un swarm amb dos nodes, un manager i un worker

 * Crear els dos nodes
 * Accedir per ssh a cada node en una terminal diferent per a cada un
 * En el node leader descarregar els apunts del curs
   git clone https://www.gitlab.com/edtasixm05/docker.git
 * Activar el swarm en el node manager
 * Afegir-se al swarm en el node worker

```
[node1] (local) root@192.168.0.13 /tmp
$ docker swarm init --advertise-addr 192.168.0.13
Swarm initialized: current node (av530kh89tl9sfm7t63x9yybx) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-260mgz2xtsjbwyow6sbvz4smyl9rsz8qsipygdogfn51rdge4j-9p5y42hd195j1i66zlo747sui 192.168.0.13:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

```
$  docker swarm join --token SWMTKN-1-260mgz2xtsjbwyow6sbvz4smyl9rsz8qsipygdogfn51rdge4j-9p5y42hd195j1i66zlo747sui 192.168.0.13:2377
This node joined a swarm as a worker.
[node2] (local) root@192.168.0.12 ~
```

Consultar informació dels nodes

```
[node1] (local) root@192.168.0.13 /tmp
$ docker node 
demote   inspect  ls       promote  ps       rm       update   

$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
av530kh89tl9sfm7t63x9yybx *   node1      Ready     Active         Leader           20.10.0
pj5qxf8rgnkzguen3mzpm0sr0     node2      Ready     Active                          20.10.0

[node1] (local) root@192.168.0.13 /tmp
$ docker node ps
ID        NAME      IMAGE     NODE      DESIRED STATE   CURRENT STATE   ERROR     PORTS
```

Els workers no poden gestionar els nodes ni el swarm
```
[node2] (local) root@192.168.0.12 ~
$ docker node ls
Error response from daemon: This node is not a swarm manager. Worker nodes can't be used to view or modify cluster state. Please run this command on a manager node or promote the current node to a manager.
```

Desplegar l'aplicació comptador de visites amb docker stack
```
$ cd docker/swarm/
[node1] (local) root@192.168.0.13 /tmp/docker/swarm

$ ls
README.md            docker-compose.yml   play-with-docker.md
[node1] (local) root@192.168.0.13 /tmp/docker/swarm

$ docker stack deploy -c docker-compose.yml visites
Creating network visites_webnet
Creating service visites_redis
Creating service visites_visualizer
Creating service visites_portainer
Creating service visites_web
[node1] (local) root@192.168.0.13 /tmp/docker/swarm
```

```
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
av530kh89tl9sfm7t63x9yybx *   node1      Ready     Active         Leader           20.10.0
pj5qxf8rgnkzguen3mzpm0sr0     node2      Ready     Active                          20.10.0
[node1] (local) root@192.168.0.13 /tmp/docker/swarm

$ docker node ps
ID             NAME                   IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
x7p6qddwn2bz   visites_portainer.1    portainer/portainer:latest        node1     Running         Running 24 seconds ago             
h9ccxaglroir   visites_redis.1        redis:latest                      node1     Running         Running 14 seconds ago             
xzzcyv4icym4   visites_visualizer.1   dockersamples/visualizer:stable   node1     Running         Running 10 seconds ago             
4enq5eeiuvth   visites_web.2          edtasixm05/getstarted:comptador   node1     Running         Running 14 seconds ago             
[node1] (local) root@192.168.0.13 /tmp/docker/swarm
```

Escalar el servei web a 6 rèpliques, ara n'hi ha dues totes al node1.
Aturar el servei portainer per fer el desplagament més lleuger.
```
$ docker service scale  visites_web=6
visites_web scaled to 6
overall progress: 6 out of 6 tasks 
1/6: running   [==================================================>] 
2/6: running   [==================================================>] 
3/6: running   [==================================================>] 
4/6: running   [==================================================>] 
5/6: running   [==================================================>] 
6/6: running   [==================================================>] 
verify: Service converged 

$ docker service rm visites_portainer
visites_portainer
[node1] (local) root@192.168.0.13 /tmp/docker/swarm
```


```
**$ docker stack ps visites**
ID             NAME                   IMAGE                             NODE      DESIRED STATE   CURRENT STATE            ERROR     PORTS
0mv2xnxi14fc   visites_redis.1        redis:latest                      node1     Running         Running 7 minutes ago              
9y259fen0z2y   visites_visualizer.1   dockersamples/visualizer:stable   node1     Running         Running 7 minutes ago              
5a3ex26ttkac   visites_web.1          edtasixm05/getstarted:comptador   node1     Running         Running 3 minutes ago              
ynwyaja54exv    \_ visites_web.1      edtasixm05/getstarted:comptador   node1     Shutdown        Shutdown 3 minutes ago             
blmj95g9vhef   visites_web.2          edtasixm05/getstarted:comptador   node2     Running         Running 2 minutes ago              
xmypd8ujpe67   visites_web.3          edtasixm05/getstarted:comptador   node2     Running         Running 3 minutes ago              
8m1ecg4nrlbx    \_ visites_web.3      edtasixm05/getstarted:comptador   node2     Shutdown        Shutdown 3 minutes ago             
xvvoh2phl02k   visites_web.4          edtasixm05/getstarted:comptador   node1     Running         Running 34 seconds ago             
tdki7dvw5blk   visites_web.5          edtasixm05/getstarted:comptador   node2     Running         Running 34 seconds ago             
br598135zedj   visites_web.6          edtasixm05/getstarted:comptador   node1     Running         Running 34 seconds ago             

$ docker stack ls 
NAME      SERVICES   ORCHESTRATOR
visites   3          Swarm
[node1] (local) root@192.168.0.13 /tmp/docker/swarm
```

**Atenció** Observar que a la web de play-with-docker a l'apartat **ports** ara apareixen els
ports oberts als que podem accedir des de l'exterior. Clicant a cada un d'ells es pot accedir
al port del node en concret. Observeu que es pot accedir al servei *tant si s'està executant en 
aquell node com si no*. S'ha aturat el servei portainer per fer el desplegament més lleuger.


```
URLs exemple:
   http://ip172-18-0-87-cabpu4p4lkkg00du5dl0-80.direct.labs.play-with-docker.com/
   http://ip172-18-0-87-cabpu4p4lkkg00du5dl0-8080.direct.labs.play-with-docker.com/
```

Accedir al visualizer i observar els dos nodes i la distribuació de serveis, en especial
el servei web, com estan repartides les seves rèpliques en els dos nodes. Accedir també
a la pàgina de visites dles dos nodes i observar com incrementa el comptador.


Modificar el desplegament:
 * usant l'opció scale: *docker service scale visites_web=nº*
 * modificant el fitxer docker-compose.yml i fent de nou el deploy amb: *docker stack deploy -c docker-compose.yml visites*


## Colocació de recursos als nodes: Labels & Placement &  Constraints

Posar etiquetes als nodes
```
docker node update --label-add lloc=local node1
docker node inspect node1
```

Indicar la colocació als services
```
  placement:
    constraints: [node.labels.lloc == local ]

  placement:
    constraints: [node.role == manager]
```

```
$ docker service create \
  --name redis_2 \
  --constraint node.platform.os==linux \
  --constraint node.labels.type==queue \
  redis:3.0.6

$ docker service create \
  --name web \
  --constraint node.labels.region==east \
  nginx:alpine

$ docker service create \
  --replicas 9 \
  --name redis_2 \
  --placement-pref spread=node.labels.datacenter \
  redis:3.0.6

$ docker service create \
  --replicas 9 \
  --name redis_2 \
  --placement-pref 'spread=node.labels.datacenter' \
  --placement-pref 'spread=node.labels.rack' \
  redis:3.0.6

$ docker service create --reserve-memory=4GB --name=too-big nginx:alpine

$ docker service create \
  --name nginx \
  --replicas 2 \
  --replicas-max-per-node 1 \
  --placement-pref 'spread=node.labels.datacenter' \
  nginx

$ docker service create \
  --replicas 3 \
  --network my-network \
  --name my-web \
  nginx
```
