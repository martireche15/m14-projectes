# SWARM

## Swarm/Nodes


Es poden desplegar aplicacions docker amb docker stack en un o més nodes que formen
un clúster que docker anomena swarm. Per defecte en els swarms es genera una xarxa
*ingress  routing mesh* que permet rèpliques de containers escoltant en un mateix port
i en diferents nodes. D'aquesta manera es poden desplegar aplicacions amb components
distribuïts entre diferents nodes.

Docker swarm genera per defecte una estructura de xarxa que permet a múltiples rèliques 
per exemple d'un container web del port 80 escoltar en el port 80 del host amfitrió o 
de cada un dels hosts (nodes) del swarm, estigui executant-s'hi el container o no.

El model de treball és:

 * Amb **docker swarm** es pot crear un swarm, afegir i eliminar nodes d'un swarm.

 * Amb **docker nodes** es pot gestionar l'estat d'un node, pausar-lo, etc.

 * Amb **docker stack** es poden desplegar aplicacions en un swarm.

 * Amb **docker service** o les configuracions dels serveis dins de **docker-compose**
   es poden definir directives de col·locació dels serveis en els nodes.

### Docker swarm

Tal i com s'ha vist es poden posar un o més nodes en un swarm per crear
un cluster de hosts que treballen onjutament. A l'inici de tot
s'ha vist com crear un swarn en el node *leader*. La mateixa instrucció
indica què cal fer en els nodes *worker* per unir-se al swarm.

Per gestionar noes i swarms convé tractar les ordres:
 
 * docker swarm

 * docker node

```
$ docker swarm 
ca          init        join        join-token  leave       unlock      unlock-key  update      
```

```
$ docker node 
demote   inspect  ls       promote  ps       rm       update   
```

```
$ docker swarm init
Swarm initialized: current node (kichazsxwsbdn9fqh235j1wal) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0746tzfpj4zkark60ovw6e6i40oa23g46sc5vh7vqq09dav11x-2r1nifbr08r27usdavz455yax 10.200.243.216:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Apagar el swarm
```
$ docker swarm leave --force
Node left the swarm.
```

### Docker node

```
$ docker swarm init
```

```
$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
wd2hos7k2jxkvioqr3zc0yobc *   i16        Ready     Active         Leader           24.0.2
```

**pause**
```
$ docker node update --availability pause i16 
i16

$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
wd2hos7k2jxkvioqr3zc0yobc *   i16        Ready     Pause          Leader           24.0.2
```

**drain**
```
docker node update --availability drain i16 
i16

ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
wd2hos7k2jxkvioqr3zc0yobc *   i16        Ready     Drain          Leader           24.0.2
```

**active**
```
docker node update --availability active i16 
i16

docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
wd2hos7k2jxkvioqr3zc0yobc *   i16        Ready     Active         Leader           24.0.2
```

## Colocació de recursos als nodes: Labels & Placement &  Constraints

#### Posar etiquetes als nodes
```
docker node update --label-add lloc=local i16
i16

docker node inspect i16
```

#### Indicar la colocació de serveis


