# Web API REST web amb autenticació OAUTH
    
Crear una seu web que implementa API-REST i autenticació OAUTH. Per fer-ho es segueix el curs de “Cibernarium” (en principi de 3h...) que construeix un exemple fictici primer i un sobre Spotify després.

Curs [cibernarium](https://cibernarium.barcelonactiva.cat/home)

Tot el procediment a part d’estar detallat en el curs està explicat al Howto següent:

+ Implementar el [HowTo-ASIX-API-Rest](https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/docs/HowTo-ASIX-API-REST.pdf) amb un aplicació web amb autenticació Oauth.

## Què és una API en REST?

### Què és una API?

Com el seu nom indica, és una interfície de programació d'aplicacions o un conjunt de funcions i procediments ofert per una biblioteca informàtica per ser utilitzat per un altre programa per interaccionar amb el programa en qüestió.

Una aplicació web dinàmica necessita tenir implementar un programari de connexió per tal de poder-se comunicar amb el servidor, d'on recull les dades que mostra als seus usuaris en pantalla. Aquest programari de connexió és el que es coneix com a API.

Es basa en el model **client-servidor** on el client és el que sol·licita obtenir els recursos o realitzar alguna operació sobre aquestes dades, mentre que el servidor és el que entrega o processa aquestes dades a sol·licitud del client.

---

### Què és REST?

Perquè un servidor web es pugui anomenar REST ha de satisfer 6 restriccions, l'ultima d'aquestes opcionals:

+ **Uniform Interface:** aquesta es la part fonamental del servei REST
	+ Identificació dels recursos a través de les URI o URL.
	+ Manipulació dels recursos a través de les seves representacions.
	+ Missatges autodescriptius que contenen tota la informació necessària.
	+ HATEOAS. El client interactua amb el servidor per complet mitjançant hipermèdia proporcionada dinàmicament per aquest segon.
+ **Client-Server:** separació entre client i servidor
	+ millora la **portabilitat** del codi font al no emmagatzemar les dades del client
	+ el servidor no es preocupa de l'estat del client, fent que aquest sigui mes **escalable**
	+ el desenvolupament pot ser independent l'un de l'altre, mentre la interficie uniforme no es vegi alterada.
+ **Stateless:** la comunicació entre client i servidor no requereix que el servidor hagi de guardar informació del client entre peticions consecutives. Com hem dit, cada missatge del client conté prou informació per satisfer la petició
+ **Cacheable:** les respostes del servidor poden guardar-se en una memòria cau.  L’objectiu és minimitzar les interaccions entre client i servidor, fent que el client accedeixi a la representació del recurs guardada en cau i millorant l’escalabilitat i rendiment del sistema
+ **Layered system:** Permet l'existència de capes intermediàries entre el client i el servidor, com servidors en cau o altres elements millorant la seguretat.
+ **Code on Demand:** El servidor pot proporcionar temporalment codi executant a la màquina del client per ampliar la funcionalitat.

Beneficis per a l'USUARI/A: un servei REST facilita enormement la càrrega de l’aplicació web al dispositiu mòbil, ja que aquesta consumeix dades de l’API externa i, per tant, no és necessari emmagatzemar cap dada a la memòria interna.

Beneficis per al PROGRAMADOR/A: oder disposar de fonts de dades de tot tipus a les quals poder accedir lliurement i permetre a l’USUARI o USUÀRIA interactuar amb aquestes dades i obtenir-ne nous resultats.

---

## Tecnologia AJAX:

Les aplicacions web dinàmiques utilitzen tecnologia AJAX per tal de fer les peticions i processar les respostes provinents del servidor REST. AJAX ens permet:

+ Llegir dades d’un servidor web (una vegada la pàgina s’ha carregat)
+ Actualitzar una pàgina web sense haver de recarregar la pàgina
+ Enviar dades a un servidor web

AJAX són les sigles d'Asynchronous JavaScript And XML. Les aplicacions AJAX poden utilitzar tant JSON com XML per transportat les dades. Aixó permet a les aplicacions web treballar amb multitud de recursos provinents de les API externes i am la resposta d'una molt lleugera.

El fet que AJAX sigui **asíncron** vol dir que les dades que són demanades es carreguen en
segon pla, sense interferir en cap cas en la presentació i el comportament de la pàgina. És a
dir, que l’aplicació carrega, en primer lloc, els arxius HTML, CSS i JavaScript al frontend de
l’usuari o usuària, i posteriorment executa les peticions de dades via AJAX. Una vegada
s’obtenen els resultats de la consulta, la pàgina s’actualitza amb les noves dades sense
necessitat d’haver-se de recarregar de nou. Normalment, el que s’actualitza són parts d’una
pàgina i no la pàgina completa.

Una possible tècnica per programar en AJAX és mitjançant el framework **jQuery**, que assisteix
el treball de qui programa en dos nivells: 
+ en el costat client, ofereix funcions JavaScript per enviar peticions al servidor via HTTP
+ en el costat servidor, processa les peticions, busca la informació i transmet la resposta de tornada al client.
---

## COM ES VINCULA REST AMB HTTP?

El protocol web de transferència de dades HTTP (HyperText Transfer Protocol) va ser
dissenyat per permetre la comunicació entre servidors i clients.

És important que coneguem quins són els mètodes principals que ens ofereix HTTP per tal de
poder treballar correctament amb aquest protocol dins les nostres peticions AJAX:

+ Mètode **GET**: s'utilitza per demanar dades d'un recurs epecífic, com ara una URI o ENDPOINT.
+ Mètode **POST**: també s’utilitza per recollir dades d’un servidor com el cas anterior, però alhora ens permet enviar dades dins la mateixa petició, per tal de crear o actualitzar un recurs del servidor. Les dades que enviem queden definides a la petició AJAX. Les dades no es passen per l’URL i, per tant, no es poden interceptar, així que POST sempre resulta un mètode més segur per realitzar les peticions més privades.
+ Mètode **PUT**: s’utilitza per enviar dades a un servidor, per tal de crear o actualitzar un recurs. PUT és la fórmula simplificada que ens permet repetir una mateixa petició POST a diferents parts del nostre codi, per no alterar-ne els resultats.
+ Mètode **DELETE**: s’utilitza per esborrar un recurs determinat.
---

~~~
L’element més important d’AJAX és l’objecte **XMLHttpRequest**. Tots els navegadors moderns
porten integrat aquest objecte de JavaScript que serveix per:

+ Actualitzar una pàgina web sense haver de recarregar la pàgina sencera.
+ Fer una petició de dades a un servidor, una vegada la pàgina s’ha carregat.
+ Rebre dades d’un servidor, una vegada la pàgina s’ha carregat.
+ Enviar dades al servidor, en el pla ocult o background de la nostra aplicació.
~~~

## JQUERY:

jQuery és una de les llibreries o frameworks més populars de JavaScript. Facilita la manipulació dels objectes  de l'HTML i simplifica les peticions AJAX.

L'API de jQuery és acceptada per tots els navegadors que coneixem i, per tant, és un recurs **fiable** que podem utilitxar a les nostres aplicacions web.

A més de les seves funcionalitats bàsiques, jQuery ofereix suport per a animacions, gestió d'esdeveniments, peticions AJAX i altres tasques comunes. La seva modularitat i extensibilitat han contribuït a la seva pervivència, tot i l'aparició de noves tecnologies.

Encara avui en dia, jQuery pot ser una elecció viable per als desenvolupadors que busquen una solució fiable i àgil per al desenvolupament frontend, tot i que és important tenir en compte altres alternatives i tendències del sector.

---

## JSON:

JSON ens permet guardar objectes JavaScript com a arxius bàsics de text i poder compartir-los entre servidors i clients a la xarxa.
Si tenim dades guardades en un objecte de JavaScript, sempre podem convertir aquest
objecte a JSON i enviar-lo al servidor. Per fer-ho, utilitzarem el mètode JSON.stringify().

+ JSON respon a les inicials de JavaScript Object Notation.
+ JSON és un format lleuger d’emmagatzematge i intercanvi de dades.
+ JSON és fàcil d’entendre, ja que només és text amb sintaxi JavaScript.
+ Quan s’intercanvien dades entre un servidor i un client, les dades només poden ser de
text.
+ JSON és una manera de treballar amb les dades directament com a objectes de JavaScript.
---

## MISSATGES DE STATUS HTTP:

Veiem ara una descripció dels missatges de resposta i error més comuns de les API.

Per fer-ho, analitzarem l’objecte XMLHttpRequest que ens retornarà el servidor cada cop que
fem una nova petició de dades:

```
XMLHttpRequest()
    onreadystatechange
    readyState
    status
```
+ La propietat **onreadystatechange** defineix la funció que ha de ser executada quan readyState canvia.
+ La propietat **readyState** conté l’estatus de la crida.
+ La propietat **status** contené els missatges d’status HTTP de l’objecte XMLHttpRequest.

Vegem els valors principals que pot prendre la propietat **readyState**:
+ **readyState** = 0 => petició no inicialitzada
+ **readyState** = 1 => connexió de servidor establerta
+ **readyState** = 2 => petició rebuda
+ **readyState** = 3 => petició en procés
+ **readyState** = 4 => petició finalitzada i resposta llesta

Ara veurem els valors més comuns que acostuma a prendre la propietat **status** en el cas que la petició AJAX es processi satisfactòriament:
+ **Status 200** => Resposta estàndard quan la petició està OK.
+ **Status 201** => En cas de peticions per POST, petició completada i nou recurs creat.

Per la seva banda, els errors 400 fan referència als errors de la part client:
+ **Status 400 = Bad Request** => La petició no es pot completar per un error de sintaxi.
+ **Status 401 = Unauthorized** => La petició és legal peròel servidor la rebutja.

Acostuma a saltar en els casos que l’autorització és necessària però errònia.
+ **Status 403 = Forbidden** => La petició és legal peròel servidor la rebutja.
+ **Status 404 = Not found** => L’URL indicat no es troba.

I els errors 500 indiquen errors de servidor:
+ **Status 500 = Internal Server Error** => Missatge d’error genèric.
+ **Status 503 = Service Unavailable** => El servidor no està disponible
---

## OAuth

OAuth (Open Authorization) és un protocol d'autorització que permet a usuaris donar accés als seus recursos sense revelar les seves credencials. Es basa en l'ús de tokens, com els "access tokens", i utilitza un flux d'autorització per a autenticació segura en aplicacions web i mòbils.

En el context d'un usuari que vol iniciar sessió en una aplicació amb l'autenticació d'OAuth de Google, el procés és el següent:

+ L'usuari sol·licita iniciar sessió i és redirigit a la pàgina d'autenticació de Google.

+ Després d'autenticar-se amb les seves credencials de Google, l'usuari dóna permís per accedir a determinades dades o funcionalitats.

+ Google emet un "access token" a l'aplicació que ha sol·licitat l'accés.

+ Aquest "access token" s'utilitza per autenticar l'usuari i accedir als recursos autoritzats sense exposar les credencials de Google.

+ L'aplicació pot utilitzar aquest "access token" per fer crides a l'API de Google en nom de l'usuari per obtenir o enviar dades.


Així, OAuth proporciona un marc segur per a l'autenticació i autorització, permetent als usuaris controlar quina informació comparteixen amb les aplicacions terceres sense haver de revelar les seves contrasenyes.

---


